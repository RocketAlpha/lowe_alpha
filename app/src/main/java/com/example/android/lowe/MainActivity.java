package com.example.android.lowe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView mTextview_login = findViewById(R.id.text_view_register_backtologin);
        final EditText mEditText_email = findViewById(R.id.edit_text_register_email);
        EditText mEditText_phone = findViewById(R.id.edit_text_register_phone);
        final EditText mEditText_password = findViewById(R.id.edit_text_register_password);
        mTextview_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginIntent();
            }
        });
        Button mButton = findViewById(R.id.button_register_register);
        final FirebaseAuth firebaseAuth =FirebaseAuth.getInstance();

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEditText_email.getText().toString().trim();
                String password = mEditText_password.getText().toString().trim();

                if(TextUtils.isEmpty(password)) {
                    mEditText_password.setError("Should not be empty");
                }
                if(TextUtils.isEmpty(email)){
                    mEditText_email.setError("Should be not be empty");
                }
                if(password.length()<6){
                    mEditText_password.setError("Length of the password should be greater 6");
                }
                firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(MainActivity.this,"User Registerd",Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),Login.class));
                        }else {
                            Toast.makeText(MainActivity.this,task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                            Log.d("Error :",task.getException().getMessage());
                        }
                    }
                });
            }
        });
    }
    protected  void  loginIntent(){
        Intent intent = new Intent(this,Login.class);
        startActivity(intent);
    }
}
